def returnUpper(s: str):
    ans = ""
    diff = ord('A') - ord('a')
    for l in s:
        if ord('a') <= ord(l) <= ord('z'):
            ans += chr(ord(l) + diff)
        else:
            ans += l
    return ans


def returnLower(s: str):
    ans = ""
    diff = ord('A') - ord('a')
    for l in s:
        if ord('A') <= ord(l) <= ord('Z'):
            ans += chr(ord(l) - diff)
        else:
            ans += l
    return ans


def IsAlpha(s: str):
    for l in s:
        if ord('a') <= ord(l) <= ord('z') or ord('A') <= ord(l) <= ord('Z'):
            continue
        else:
            return False
    return True


def IsDigit(s: str):
    for l in s:
        if ord('0') <= ord(l) <= ord('9'):
            continue
        else:
            return False
    return True


def myIsOther(s: str):
    if IsAlpha(s) or IsDigit(s):
        return False
    return True


test_all = ["AbcD!e", "!@$%^", "123", "abc"]
for test in test_all:
    print("str:", test)
    print("returnUpper", returnUpper(test))
    print("returnLower", returnLower(test))
    print("IsAlpha", IsAlpha(test))
    print("IsDigit", IsDigit(test))
    print("myIsOther", myIsOther(test))
    print()